package org.example;

public class HinhThang extends Shape{
    private float a;
    private float b;
    private float c;
    private float d;

    private float h;

    public HinhThang(float a, float b, float c, float d, float h) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.h = h;
    }
    @Override
    public void perimeter() {
        float C = a + b + c + d;
        System.out.println("Chu vi hình thang là: " + C);
    }

    @Override
    public void area() {
        float C = (a + c)*h/2;
        System.out.println("Diên tích hình thang là: " + C);
    }
}
