package org.example;

public class HinhVuong extends Shape{
    private float a;
    public HinhVuong(float a) {
        this.a = a;
    }

    @Override
    public void perimeter() {
        float C = a+a+a+a;
        System.out.println("Chu vi hình vuông là: "+C);
    }

    @Override
    public void area() {
        float S = a*a;
        System.out.println("Diện tích hình vuông là: "+S);
    }
}
