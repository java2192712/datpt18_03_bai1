package org.example;

public abstract class Shape {
    private String name;
    public abstract void perimeter();
    public abstract void area();
}
