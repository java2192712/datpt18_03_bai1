package org.example;

public class HinhTron extends Shape{
    private double r;

    public HinhTron(double r) {
        this.r = r;
    }

    @Override
    public void perimeter() {
        double C = r*2*Math.PI;
        System.out.println("Chu vi hình tròn là: " + C);
    }
    @Override
    public void area() {
        double S = r*r*Math.PI;
        System.out.println("Diện tịch hình tròn là: " + S);
    }
}
