package org.example;

public class HinhChuNhat extends Shape{
    private float a;

    public HinhChuNhat(float a, float b) {
        this.a = a;
        this.b = b;
    }

    private float b;
    @Override
    public void perimeter() {
        float C = (a+b)*2;
        System.out.println("Chu vi hình chũ nhật là: " + C);

    }

    @Override
    public void area() {
        float S = a*b;
        System.out.println("Diện tích hình chũ nhật là: " + S);
    }
}
