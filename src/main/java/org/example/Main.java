package org.example;

public class Main {

    public static void main(String[] args) {
        HinhTron hinhTron = new HinhTron(3);
        hinhTron.perimeter();
        hinhTron.area();
        System.out.println();

        HinhVuong hinhVuong = new HinhVuong(3);
        hinhVuong.perimeter();
        hinhVuong.area();
        System.out.println();

        HinhChuNhat hinhChuNhat =new HinhChuNhat(3,4);
        hinhChuNhat.perimeter();
        hinhChuNhat.area();
        System.out.println();

        HinhThang hinhThang = new HinhThang(8, 6, 10, 7, 5);
        hinhThang.perimeter();
        hinhThang.area();


    }
}